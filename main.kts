package converter

import java.util.*

enum class ConversionType {
    MASS,
    LENGTH,
    TEMPERATURE,
    NULL
}

fun pluralWord(count: Double, word: String) = when {
    count != 1.0 && word.endsWith('s') -> "${word}es"
    count != 1.0 && word == "foot" -> "feet"
    count != 1.0 -> "${word}s"
    else -> word
}

enum class Units(val singular: String,val short: String ,val shortest: String, val type: ConversionType) {
    GRAM("gram", "g", "", ConversionType.MASS),
    KILOGRAM("kilogram", "kg", "", ConversionType.MASS),
    MILLIGRAM("milligram", "mg", "", ConversionType.MASS),
    POUND("pound", "lb", "", ConversionType.MASS),
    OUNCE("ounce", "oz", "", ConversionType.MASS),
    METER("meter", "m", "", ConversionType.LENGTH),
    YARD("yard", "ya", "", ConversionType.LENGTH),
    CENTIMETER("centimeter", "cm", "", ConversionType.LENGTH),
    KILOMETER("kilometer", "km", "", ConversionType.LENGTH),
    FOOT("foot", "ft", "", ConversionType.LENGTH),
    INCH("inch", "in", "", ConversionType.LENGTH),
    MILLIMETER("millimeter", "mm", "", ConversionType.LENGTH),
    CELSIUS("celsius", "dc", "c", ConversionType.TEMPERATURE),
    KELVIN("kelvin", "kelvin", "k", ConversionType.TEMPERATURE),
    FAHRENHEIT("fahrenheit", "df", "f", ConversionType.TEMPERATURE),
    NULL("", "", "???", ConversionType.NULL);

    companion object {
        fun plural(unit: Units, count: Double): String {
            if(unit == NULL) return "???"

            if(unit.type == ConversionType.TEMPERATURE && unit != KELVIN) {
                return "${pluralWord(count,"degree")} ${unit.singular.capitalize()}"
            } else if (unit == KELVIN) {
                return pluralWord(count, "Kelvin")
            }

            return pluralWord(count, unit.singular)
        }

        fun findUnit(unit: String): Units {
            for(value in values()) {
                if(value.short == unit || value.singular == unit || value.shortest == unit || unit == pluralWord(2.0, value.singular)) {
                    return value
                }
            }

            return NULL
        }
    }
}

fun convertLengthAndMass(what: Units, value: Double, reverse: Boolean = false): Double {
    val coefficient = when(what) {
        Units.GRAM -> 1.0
        Units.KILOGRAM -> 1000.0
        Units.MILLIGRAM -> 0.001
        Units.POUND -> 453.592
        Units.OUNCE -> 28.3495
        Units.METER -> 1.0
        Units.YARD -> 0.9144
        Units.CENTIMETER -> 0.01
        Units.FOOT -> 0.3048
        Units.INCH -> 0.0254
        Units.MILLIMETER -> 0.001
        Units.KILOMETER -> 1000.0
        else -> 1.0
    }

    return if (reverse) value / coefficient else value * coefficient
}

fun convertTemperature(value: Double, from: Units, to: Units): Double = when {
    from == Units.KELVIN && to == Units.CELSIUS -> value - 273.15
    from == Units.KELVIN && to == Units.FAHRENHEIT -> value * 9 / 5 - 459.67
    from == Units.CELSIUS && to == Units.KELVIN -> value + 273.15
    from == Units.CELSIUS && to == Units.FAHRENHEIT -> value * 9 / 5 + 32
    from == Units.FAHRENHEIT && to == Units.CELSIUS -> (value - 32) * 5 / 9
    from == Units.FAHRENHEIT && to == Units.KELVIN -> (value + 459.67) * 5 / 9
    else -> value
}

class Converter(val firstValue: Double, firstUnit: String, secondUnit: String) {
    val firstUnit = Units.findUnit(firstUnit)

    val secondUnit = Units.findUnit(secondUnit)

    private fun printResult(secondValue: Double) {
        println("$firstValue ${Units.plural(firstUnit, firstValue)} is $secondValue ${Units.plural(secondUnit, secondValue)}")
    }

    fun convert() {
        if (firstUnit == Units.NULL || secondUnit == Units.NULL || firstUnit.type != secondUnit.type) {
            println("Conversion from ${Units.plural(firstUnit, 2.0)} to ${Units.plural(secondUnit, 2.0)} is impossible")
            return
        }

        if (firstUnit.type == ConversionType.MASS && firstValue < 0) {
            println("Weight shouldn't be negative")
            return
        }

        if (firstUnit.type == ConversionType.LENGTH && firstValue < 0) {
            println("Length shouldn't be negative")
            return
        }

        val secondValue = when(firstUnit.type) {
            ConversionType.LENGTH, ConversionType.MASS -> {
                convertLengthAndMass(secondUnit, convertLengthAndMass(firstUnit, firstValue), true)
            }
            ConversionType.TEMPERATURE -> convertTemperature(firstValue, firstUnit, secondUnit)
            ConversionType.NULL -> 1.0
        }

        printResult(secondValue)
    }
}

fun main() {
    val scanner = Scanner(System.`in`)

    while(true) {
        println("Enter what you want to convert (or exit):")

        if (!scanner.hasNextDouble()) {
            if(scanner.next() == "exit") {
                break
            }
            println("Parse error")
            break
        }

        val firstValue = scanner.nextDouble()

        var firstUnit = scanner.next()
        if (firstUnit.toLowerCase() in "degree" .. "degrees") {
            firstUnit = scanner.next()
        }
        scanner.next()

        var secondUnit = scanner.next()
        if (secondUnit.toLowerCase() in "degree" .. "degrees") {
            secondUnit = scanner.next()
        }

        val converter = Converter(firstValue, firstUnit.toLowerCase(), secondUnit.toLowerCase())
        converter.convert()
    }
}
